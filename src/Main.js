import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';
import { connect } from 'react-redux';
import { setMessages } from './store/actions/app';
import Login from './components/screens/Login';

class Main extends Component {
    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Login></Login>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        msgs: state.app.messages,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setMsgs: (msg) => dispatch(setMessages(msg))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main)
