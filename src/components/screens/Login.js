import React, { Component } from 'react';
import { View, Button } from 'react-native';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import CTextInput from '../core/CTextInput';
import { loginAction } from '../../store/actions/app';

class Login extends Component {

    onSubmit = (values) => {
        console.log('Submitted !!! ' + JSON.stringify(values));
        this.props.onLogin({...values, token: 'DUMMY_TOKEN'});
    }

    render() {
        return (
            <View>
                <Field 
                    name="username"
                    component={CTextInput} 
                    placeholder="Username"
                    />
                 <Field 
                    name="password"
                    component={CTextInput} 
                    placeholder="Password"
                    />
                <Button title="Sign in" onPress={this.props.handleSubmit(this.onSubmit)}/>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {
        onLogin: ({ username, password, token }) =>
            dispatch(loginAction(username, password, token))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    reduxForm({
        form: 'Login',
        validate: values => {
            const errors = {};
            errors.username = !values.username
                ? 'รหัสผู้ใช้งาน/Username is required'
                : undefined;

            errors.password = !values.password
                ? 'รหัสผ่าน/Password is required'
                : undefined;

            return errors;
        }
    })(Login)
);