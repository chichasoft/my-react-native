import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import CText from './CText';

class CButton extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <View style={this.props.style}>
                    <CText text={this.props.title} color="white"/>
                </View>
            </TouchableOpacity>
        );
    }
}

export default CButton;
