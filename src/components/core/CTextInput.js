import React, { Component } from 'react';
import { TextInput, StyleSheet, View, Text } from 'react-native';

class CTextInput extends Component {
    render() {
        const { input, meta, ...inputProps } = this.props;
        console.log(JSON.stringify(meta));
        return (
            <View>
                <TextInput
                    {...inputProps}
                    value={input.value}
                    onChangeText={input.onChange}
                    onBlur={input.onBlur}
                    onFocus={input.onFocus}
                    style={styles.textInput}
                />
                <Text style={{color: 'red'}}>{meta.touched ? meta.error : ''}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textInput : {
        borderWidth: 1,
        padding: 7,
        margin : 10,
        marginBottom: 0,
        width: 200,
    }
});

export default CTextInput;
