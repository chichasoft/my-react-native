import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';

class CText extends Component {
    state = {

    }

    render() {
        return (
            <View>
                <Text style={[styles.text, {color: this.props.color}]}>
                {this.props.text}
                </Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    text: {
        fontSize: 20,
        color: 'gray'
    }
});
export default CText;
