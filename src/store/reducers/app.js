import { SET_MESSAGE, CLEAR_MESSAGE, LOGIN_SUCCESS, LOGIN_ERROR } from "../actions/actionTypes";

const initialState = {
    messages: [],
    loginRespData: null,
    errorMessage: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_MESSAGE :
            return {
                ...state,
                messages: [action.payload]
            }
        case CLEAR_MESSAGE :
            return {
                ...state,
                messages: []
            }
        case LOGIN_SUCCESS :
            return {
                ...state,
                loginRespData: action.payload
            }
        case LOGIN_ERROR :
            return {
                ...state,
                errorMessage: action.payload
            }
        default: 
            return state;
    }
}

export default reducer;
