import {
    SET_MESSAGE,
    CLEAR_MESSAGE,
    LOGIN_START,
    LOGIN_SUCCESS,
    LOGIN_ERROR
} from './actionTypes';

export const setMessages = message => {
    return {
        type: SET_MESSAGE,
        payload: message
    };
};

export const clearMessages = () => {
    return {
        type: CLEAR_MESSAGE
    };
};

export const loginStart = (username, password) => {
    return {
        type: LOGIN_START,
        payload: { username, password }
    };
};

export const loginSuccess = respData => {
    return {
        type: LOGIN_SUCCESS,
        payload: respData
    };
};

export const loginError = error => {
    return {
        type: LOGIN_ERROR,
        payload: error
    };
};

export const loginAction = (username, password, token) => {
    return async dispatch => {
        dispatch(loginStart(username, password));
        try {
            const resp = await fetch(
                'http://175.176.223.206/iserviceapi/api/Account/Login',
                {
                    method: 'post',
                    headers: new Headers({
                        'Content-Type': 'application/json',
                        'Transfer-Encoding': 'chunked'
                    }),
                    body: JSON.stringify({
                        user_name: username,
                        password: password,
                        notify_token: token
                    })
                }
            );

            const data = await resp.json();
            if (data.status === 200) {
                dispatch(loginSuccess(data.data));
            } else {
                dispatch(loginError(data.message));
            }
        } catch (err) {
            dispatch(loginError('Unexpected Error : ' + err))
        }
    };
};
